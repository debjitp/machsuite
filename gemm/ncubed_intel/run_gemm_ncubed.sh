#!/bin/bash
DESIGN=gemm
SUBDESIGN=${DESIGN}_ncubed
DATA_DIR=./${SUBDESIGN}_test/
PROJ_DIR=${DATA_DIR}_intel/
FUNCT_DIR=./${SUBDESIGN}_funct/
VERI_DIR=./${SUBDESIGN}.prj/verification

source /work/shared/common/Intel/intelFPGA_pro/17.1/hls/init_hls.sh

i++ -v ${DESIGN}.c -march=Cyclone10GX --quartus-compile --clock 5ns --simulator none
